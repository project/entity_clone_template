Entity Clone Template
===========

Add a Drupal View to clone a content from a template.
 
Installation
------------

* Install as you would normally install a contributed Drupal module.
  See: https://www.drupal.org/node/895232 for further information.
  
Pre-requisites
--------------
For convenience, you can use the following patch to be redirected to the node
edit page after a clone:
https://www.drupal.org/files/issues/2019-03-11/improve-workflow-of-cloned-entity-2708731-11_1.patch

Usage
------------
* After enabling the module, make sure at least one content type is configured
to use Entity Clone Template.
For example, for the article content type
go to: `[SITE]/admin/structure/types/manage/article` then check
**Enable Entity Clone Template** under **Entity Clone Content** tab.

* When creating or editing a content, you should now have an
**Entity Clone Template** section where you can define the current content as a
content template and set an image to be displayed on the Entity Clone Template
view page.

* The Entity Clone Template view page is available at
`[SITE]/admin/content/clone-content-from-template` or under the **Content**
Admin Menu.
